#!/usr/bin/env python

import os, shutil
import subprocess
import uuid
from contextlib import contextmanager

class ShellUtilsException(Exception):
    pass

@contextmanager
def cd(directory):
    """contextmanager for chdir"""
    try:
        curDir = os.path.abspath(os.getcwd())
    except OSError:
        curDir = "/"
    os.chdir(directory)
    yield
    try:
        os.chdir(curDir)
    except OSError:
        pass

class tmpDir(object):
    """Tmp dir creation with automatic clean-up"""
    def __init__(self):
        self.uuid = str(uuid.uuid4())
        self.path = "/tmp/{}".format(self.uuid)

    def __enter__(self):
        os.makedirs(self.path)
        return self.path

    def __exit__(self, *args, **kwargs):
        shutil.rmtree(self.path)
        return
        
@contextmanager
def cdTmp():
    """ Work in a temporary directory which should be cleaned up after leaving the context"""
    with tmpDir() as f:
        with cd(f):
            yield f

def allFiles(directory):
    allFiles = []
    for root, dirs, files in os.walk(directory):
        allFiles += files
    return allFiles

def hostname():
    """ return hostname of machine """
    p = subprocess.Popen(["hostname"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout, stderr = p.communicate()
    if stderr:
        raise ShellUtilsException("calling hostname raises stderr: {}".format(stderr))
    return "{}".format(stdout.strip())

def programAvailable(programName):
    """ Return if program is available to subprocess """
    p = subprocess.Popen(["which", programName], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout, stderr = p.communicate()
    return p.returncode == 0

if __name__ == '__main__':
    print(os.listdir())
    with cd("/tmp"):
        print(os.listdir())
    print(os.listdir())
        
    with tmpDir() as f:
        print(os.listdir(f))
        print(f)
    print(os.listdir("/tmp"))

    with cdTmp() as tmpDir:
        print(tmpDir)
        with open(os.path.join("/tmp", "testFile.txt"), "w") as f:
            f.write("test")
