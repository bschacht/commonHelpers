#!/usr/bin/env python
from __future__ import print_function

import re
import logging
logger = logging.getLogger(__name__)

def AODtoDAOD(DID, derivation, pTag):
    """ replace DID from AOD type to DAOD """
    if derivation[:5] != 'DAOD_':
        derivation = 'DAOD_' + derivation
    return re.sub('AOD', derivation, DID) + getTag(pTag,'p')

def DAODtoAOD(DID):
    """ get AOD from DAOD """
    DID, count = re.subn('DAOD_\w{4}\d+', 'AOD', DID)
    if count != 1: logger.warning("{} couldn't be replaced to AOD".format(DID))
    DID, count = re.subn('_p\d{4}', '', DID)
    if count != 1: logger.warning("pTag removal failed {}".format(DID))
    return DID

def getTag(tag, character):
    """ provide consistent x-tags """
    if re.match('{}\d{{4}}'.format(character), tag) is not None:
        newTag = '_' + tag
    elif re.match('\d{4}', tag) is not None:
        newTag = '_{}'.format(character) + tag
    else:
        logger.warning("couldn't construct correct {}-tag from {}".format(character, tag))
        return None
    return newTag

def replaceRTag(DID, rTag):
    rTag = getTag(rTag, 'r')
    newDID, count = re.subn('_r\d{4}_r', rTag + '_r', DID)
    if count != 1: logger.warning("r-tag {} replacement failed {} {}".format(rTag, DID, newDID))
    return newDID

def replacePTag(DID, pTag):
    pTag = getTag(pTag, 'p')
    newDID, count = re.subn('_p\d{4}', '_' + pTag, DID)
    if count != 1: logger.warning("p-tag {} replacement failed {} {}".format(pTag, DID, newDID))
    return newDID

def extractID(DID):
    search = re.search("(\d{8})", DID)
    if search is None:
        search = re.search("(\d{6})", DID)
    if search is not None:
        return search.group(1)
    else:
        logger.warning("could not extract ID from {}".format(DID))

def extractPTag(DID):
    tags = extractTags(DID)
    return tags['p']

def extractRecoMergeTag(DID):
    tags = extractTags(DID)
    if 'a' in tags:
        return tags['a'][-1], tags['r'][0]
    else:
        return tags['r'][0], tags['r'][1]

def getIDgroups(DIDs):
    info = {}
    for DID in DIDs:
        ID = extractID(DID)
        try:
            info[ID].append(DID)
        except KeyError:
            info[ID] = [DID]
    return info

def extractScope(name):
    search = re.search('(data\d{2}_\d{2}TeV)', name)
    if search is None:
        search = re.search('(mc\d{2}_\d{2}TeV)', name)
    if search is None:
        search = re.search('^(user.\w*).', name)
    if search is not None:
        return search.group(1)
    else:
        logger.warning("could not extract Scope from {}".format(name))

def extractTags(name):
    possibleTags = [('f', 3), ('m', 4), ('e', 4), ('s', 4), ('a', 3), ('r', 4), ('p', 4)]
    extractedTags = {}
    for character, num in possibleTags:
        search = re.findall('({}\d{{{}}})'.format(character, num), name)
        for result in search:
            try:
                extractedTags[character].append(result)
            except KeyError:
                extractedTags[character] = [result]
    return extractedTags

def extractDataType(DID):
    search = re.search('(DAOD_\w{5})', DID)
    if search is None:
        search = re.search('(AOD)', DID)
    if search is not None:
        return search.group(1)
    else:
        logger.warning("could not extract dataType from {}".format(DID))

def getMC15cCandidates(DID):
    """ Experimental function to determine MC15c candidate DIDs """
    import ami
    import myRucio.getDIDs as GD
    # from rucio import RucioIDContainers
    ID = extractID(DID)
    # print (DID, ID)
    cont = GD.RucioIDContainers('mc15_13TeV', ID)
    scDict = {}
    for candidate in cont:
        if re.search('merge.AOD', candidate) is None:
            continue
        try:
            sc = mcSubcampaign(candidate, silent=True)
        except KeyError:
            sc = "not matched"
        try:
            scDict[sc].append(candidate)
        except KeyError:
            scDict[sc] = [candidate]

    if not 'mc15c' in scDict:
        logger.error("no mc15c candidate for {}".format(DID))
        return None
    if len(scDict['mc15c']) == 1:
        return scDict['mc15c']
    else:
        logger.warning('multiple candidates: {}'.format(', '.join(scDict['mc15c'])))
        return scDict['mc15c']

def isData(DID):
    ds = re.search('data', DID)
    dm = re.search('mc', DID)
    if ds is not None and dm is None:
        return True
    if ds is None and dm is not None:
        return False
    logger.error("could not determine data/mc: {}".format(DID))
    raise

def mcSubcampaign(DID, **kwargs):
    import json
    import os
    baseDirectory = os.path.dirname(os.path.abspath(__file__)) + '/'

    with open(baseDirectory + "../helpers/recoTags.json", 'r') as f:
        recoTags = json.load(f)
    with open(baseDirectory + "../helpers/mergeTags.json", 'r') as f:
        mergeTags = json.load(f)
    sc = None
    recoTag, mergeTag = extractRecoMergeTag(DID)
    try:
        sc = recoTags[recoTag]
    except KeyError:
        if not kwargs.get("silent", False):
            logger.error("reco tag {} not in database: {}".format(recoTag, recoTags))

    if not sc in mergeTags[mergeTag]:
        logger.error("merge tag {} does not match subcampaign: {}".format(mergeTag, mergeTags[sc]))
        raise KeyError
    return sc


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Do Manipulations with DID names')
    mode = parser.add_mutually_exclusive_group(required=True)
    mode.add_argument('-D', '--AODtoDAOD', action='store_true', help='Convert AOD to DAOD')
    mode.add_argument('-A', '--DAODtoAOD', action='store_true', help='Convert DAOD to AOD')
    mode.add_argument('-R', '--replaceRTag', action='store_true', help='replace r-tag')
    mode.add_argument('-I', '--extractID', action='store_true', help='extract ID')
    mode.add_argument('-S', '--subcampaign', action='store_true', help='extract subcampaign')
    mode.add_argument('--mc15cCandidates', action='store_true', help='get mc15c candidates')

    parser.add_argument('-d', '--derivation', default='SUSY5', help='supply derivation')
    parser.add_argument('-p', '--pTag', default='p2470', help='supply pTag')
    parser.add_argument('-r', '--rTag', default='r7267', help='supply rTag')
    parser.add_argument('-f', '--input', type = argparse.FileType('r'), default = '-')

    args = parser.parse_args()

    for line in args.input:
        line = line.strip()
        if args.AODtoDAOD:
            print(AODtoDAOD(line, args.derivation, args.pTag))
        if args.DAODtoAOD:
            print(DAODtoAOD(line))
        if args.replaceRTag:
            print(replaceRTag(line, args.rTag))
        if args.extractID:
            print(extractID(line))
        if args.subcampaign:
            print(mcSubcampaign(line), line)
        if args.mc15cCandidates:
            cand = getMC15cCandidates(line)
            if cand is not None:
                for c in cand:
                    print(c)
