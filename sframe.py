#!/usr/bin/env python
import os

def expectedFile(xmlFile):
    """ return dictionary with expected output files for each Cycle and """
    try:
    	from lxml import etree
    except ImportError:
    	import xml.etree.ElementTree as etree
    parser = etree.XMLParser(remove_comments=True)
    parsed = etree.parse(xmlFile, parser=parser)
    root = parsed.getroot()
    # print root.tag
    files = {}
    for child in root:
        # print child, child.keys()
        if 'OutputDirectory' in child.keys():
            files[child.attrib['Name']] = []
            for grandchild in child:
                # print "gc", grandchild
                if grandchild.tag == "InputData":
                    files[child.attrib['Name']].append(os.path.join(child.attrib['OutputDirectory'], "{}.".format(child.attrib['Name']) + grandchild.attrib['Type'] + '.' + grandchild.attrib['Version'] + '.root'))
        if child.attrib['Name'] == 'StopStauCycle':
            # print child.attrib['OutputDirectory']
            for grandchild in child:
                try:
                    files.append((os.path.join(child.attrib['OutputDirectory'], "StopStauCycle." + grandchild.attrib['Type'] + '.' + grandchild.attrib['Version'] + '.root'), xmlFile))
                except KeyError:
                    pass
    return files

def expectedFiles(XMLDirectory):
    """ Get from a directory containing SFrame XMLs expected output root files """
    files = []
    for f in os.listdir(XMLDirectory):
        for item in expectedFile(os.path.join(XMLDirectory, f)).values():
            files += item
    return files

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='SFrame helper')
    parser.add_argument('-e', '--expectedFiles', action='store_true', help='print expected files')
    parser.add_argument('-x', '--XMLFile', help='xmlFile to be investigated')
    
    args = parser.parse_args()

    if args.expectedFiles:
        for c, files in expectedFile(args.XMLFile).items():
            for f in files:
                print "{}:{}".format(c, f)

    # print(expectedFiles("testFiles/xmlFolder"))
