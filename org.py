#!/usr/bin/env python
import os, subprocess
import logging
logger = logging.getLogger(__name__)
import shutil

from commonHelpers.shellUtils import cdTmp
from commonHelpers.html.htmlWriter import Tag

from commonHelpers.memeMock import cache

@cache
def getHtmlFromOrg(orgFile, htmlFile=None):
    """Provide a html file, get back path to html file"""
    if htmlFile is None:
        htmlFile = "/tmp/" + os.path.basename(orgFile)[:-4] + '.html'

    with cdTmp() as tmpDir:
        shutil.copy(orgFile, tmpDir)
        tmpOrgFile = os.path.join(tmpDir, os.path.basename(orgFile))
        tmpHtmlFile = tmpOrgFile[:-4] + '.html'

        p = subprocess.Popen(['emacs', tmpOrgFile, '--batch', '-f', 'org-html-export-to-html'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
        stdout, stderr = p.communicate()
        # print(args, stdout, stderr)
        

        if stderr:
            logger.error("{} on {} with".format(stderr, tmpOrgFile))
            # raise ValueError()

        shutil.copy(tmpHtmlFile, htmlFile)

        return htmlFile

if __name__ == '__main__':

    import argparse
    parser = argparse.ArgumentParser(description='Transform org file to html')
    parser.add_argument('orgFile', help='supply path to org file')
    parser.add_argument('-o', '--outFile', help='output html')
    
    args = parser.parse_args()

    print(getHtmlFromOrg(args.orgFile, htmlFile=args.outFile))
