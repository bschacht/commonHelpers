#!/usr/bin/env python
import os, subprocess
import datetime
import logging
logger = logging.getLogger(__name__)

from commonHelpers.shellUtils import cd
from commonHelpers.html.htmlWriter import Tag

class GitException(Exception):
    pass

class Git(object):
    def __init__(self, path):
        self.path = os.path.expanduser(path)

    def init(self):
        with cd(self.path):
            p = subprocess.Popen(["git", "init"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.communicate()
            return p.returncode == 0
            

    def isGit(self):
        logger.debug("check if there is a git in {}".format(self.path))
        if not os.path.exists(self.path):
            return False
        with cd(self.path):
            p = subprocess.Popen(["git", "rev-parse"], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            p.communicate()
            return p.returncode == 0

    def __str__(self):
        s = "{} ".format(self.path)
        if not self.isGit():
            s += "(no git found)"
            return s

        s += "on {}".format(self.currentBranch())

        ah, bh = self.aheadBehind()
        s += " ({} ahead, {} behind)".format(ah, bh)
        return s

    def html(self):
        t = ""

    def call(self, *args, **kwargs):
        """Helper function to call git commands and to return stdout.

        Kwargs:
         ignoreErrors: do not raise on stderr output
        """

        with cd(self.path):
            p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            stdout, stderr = p.communicate()
            # print(args, stdout, stderr)
            if stderr and not kwargs.get("ignoreErrors", False):
                raise GitException("{} on {} with".format(stderr, self.path, args))
            return "{}".format(stdout.strip())

    def add(self, *files):
        """Add files to git"""
        return self.call('git', 'add', *files)

    def commit(self, message):
        """Add files to git"""
        return self.call('git', 'commit', '-m', message)

    def status(self):
        with cd(self.path):
            p = subprocess.Popen(["git", "status", '--porcelain', '-bu'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
            stdout, stderr = p.communicate()
            if stderr:
                raise GitException("{} on {}".format(stderr, self.path))
            statusDict = {}
            for line in stdout.split("\n"):
                if not line:
                    continue
                # print (line)
                if line[:2] == "##":
                    if 'branchExpr' in statusDict:
                        raise GitException('branchExpr {} already in statusDict'.format(line))
                    statusDict['branchExpr'] = line[3:]
                elif line[:2] == "??":
                    statusDict.setdefault("untrackedFiles", []).append(line[3:])
                elif line[:2] == "M ":
                    statusDict.setdefault("modifiedFiles", []).append(line[3:])
                elif line[:2] == " M":
                    statusDict.setdefault("ModifiedFiles", []).append(line[3:])
                elif line[:2] == "MM":
                    statusDict.setdefault("modifiedStagedFiles", []).append(line[3:])
                elif line[:2] == "R ":
                    statusDict.setdefault("renamedFiles", []).append(line[3:])
                elif line[:2] == "RM":
                    statusDict.setdefault("renamedModifiedFiles", []).append(line[3:])
                elif line[:2] == "A ":
                    statusDict.setdefault("addedFiles", []).append(line[3:])
                else:
                    raise GitException('line {} not matched for statusDict'.format(line))
            return statusDict

    def currentBranch(self):
        return self.call("git", "symbolic-ref", '--short', 'HEAD')

    def currentRemote(self):
        return self.call("git", "config", "branch.{}.remote".format(self.currentBranch()))

    def currentMergeBranch(self):
        return self.call("git", "config", "branch.{}.merge".format(self.currentBranch())).split("/")[2]

    def currentCommit(self):
        try:
            return self.call("git", "rev-list", "-n1", "HEAD", "--pretty=oneline", "--abbrev-commit")
        except GitException:
            return None

    def aheadBehind(self):
        if not self.currentRemote():
            return None, None
        stdout = self.call("git", "rev-list", "--left-right", "--count", "{}...{}/{}".format(self.currentBranch(), self.currentRemote(), self.currentMergeBranch()))
        return tuple(stdout.split("\t"))

    def fetchAll(self):
        """ Do git fetch all in repo """
        if self.currentRemote():
            self.call("git", "fetch", '-a', ignoreErrors=True)

    def lastFetch(self):
        """ get last fetch of a repo """
        if self.currentRemote():
            uts = self.call("stat", "-c", '%Y', '.git/FETCH_HEAD')
        else:
            return None
        return datetime.datetime.today()- datetime.datetime.fromtimestamp(int(uts))


if __name__ == '__main__':

    g = Git(os.getcwd())
    print(g.isGit())
    g.fetchAll()
