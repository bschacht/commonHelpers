#!/usr/bin/env python
"""
Module taking care of reading and cleaning input
"""

def removeComments(lines):
    """ Yields stripped lines which are not empty and do not start with # """
    for line in lines:
        line = line.strip()
        if line[:1] == '#':
            continue
        if line == '':
            continue
        yield line.split("#")[0]


if __name__ == '__main__':
    commentedLines = [
        '',
        '# text',
        '   ',
        'text',
        '    # test',
        '\n'
    ]

    print('With comments:')
    for line in commentedLines:
        print(line)

    print('Without comments:')
    for line in removeComments(commentedLines):
        print(line)
