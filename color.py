colors = {"black": 0,
          "red": 1,
          "green": 2,
          "yellow": 3,
          "blue": 4,
          "magenta": 5,
          "cyan": 6,
          "white": 7}

def color(text, color):
    return "\033[1;3{}m".format(colors[color]) + str(text) + "\033[1;0m"

def green(text):
    return "\033[1;32m" + text + "\033[1;0m"

if __name__ == "__main__":
    print("{} {} {}".format("black", color("green", "green"), "black"))
    print("{}\n{}\n{}".format("black", color("green", "green"), "black"))
    print("{}{}{}".format("black", color("green", "green"), "black"))
