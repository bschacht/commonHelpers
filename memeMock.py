import logging
logger = logging.getLogger(__name__)

def dummyDecorator(*arx, **kwarx):
    """ function to be used if meme is an optional dependency """
    def wrapper(f):
        def decorator(*args, **kwargs):
            kwargs.pop("refreshCache",None)
            return f(*args, **kwargs)
        return decorator
    return wrapper

# try to import meme, else set dummy decorator
try:
    from meme import cache
except ImportError:
    logger.info("fall back to dummy decorator")
    cache = dummyDecorator

if __name__ == '__main__':

    @cache()
    def test(**kwargs):
        return kwargs

    assert test() == {}
    assert test(x=5) == {"x":5}
    assert test(refreshCache=True) == {}
    logger.info("passed tests")
