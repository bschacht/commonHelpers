#!/usr/bin/env python
#
# functions useful for user interaction
#

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

class outputHelper(object):
    def __init__(self, rows):
        self.rows = rows
    def printTable(self):
        printTable(self.rows)

def promptYN(text, trials=3): # This function has problems when reading from stdin
    print (text + '[yn]')
    if trials == 0:
        return False
    choice = input()
    if choice == 'y':
        return True
    elif choice == 'n':
        return False
    else:
        return promptYN('could not recognize answer, try again:' + text, trials-1)

def promptInput(message):
    print(message)
    text = raw_input()
    if text == '':
        if promptYN('add empty field?'):
            return text
        else:
            promptInput(message)
    else:
        return text

def humanReadableSize(size, suffix='B'):
    # From http://stackoverflow.com/a/1094933
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(size) < 1024.0:
            return "%3.1f%s%s" % (size, unit, suffix)
        size /= 1024.0
    return "%.1f%s%s" % (size, 'Yi', suffix)

def printTable(table, width=180):
    """ takes list of rows and turns it to printable string """
    if len(table) == 0:
        return "(empty table)"
    nColumns = len(table[0])
    maxWidth = [0] * nColumns

    # convert table to strings
    for i, row in enumerate(table):
        for j, field in enumerate(row):
            table[i][j] = str(table[i][j])
    for row in table:
        if len(row) != nColumns:
            logger.error("{} does not have {} columns".format(row, nColumns))
            raise IndexError
        for i, field in enumerate(row):
            if len(field) > maxWidth[i]:
                maxWidth[i] = len(field)

    if width is not None:
        while sum(maxWidth) > width:
            m = max(maxWidth)
            for i in range(len(maxWidth)):
                if maxWidth[i] == m:
                    maxWidth[i] -= 1
                    break

    spacer = '{:{fill}^{width}}\n'.format('', fill="#", width=sum(maxWidth)+nColumns*2)
    text = spacer
    for row in table:
        for i, field in enumerate(row):
            text += ' {:<{width}.{width}}'.format(field, width=maxWidth[i]+1)
        text += '\n'
    text += spacer
    
    return text

def tableFromDicts(dictionaries, ignoreKeys=[], **kwargs):
    keys = []
    for d in dictionaries:
        for key in d:
            if key not in ignoreKeys and key not in keys:
                keys.append(key)
    rows = []
    headRow = []
    for key in keys:
        headRow.append(key)
    rows.append(headRow)
    for d in dictionaries:
        row = []
        for key in keys:
            row.append(d.get(key, '--'))
        rows.append(row[:])
    return rows

def printDicts(dictionaries, ignoreKeys=[], **kwargs):
    return printTable(tableFromDicts(dictionaries, ignoreKeys, **kwargs), **kwargs)

def humanTimeDiff(timeDiff):
    """take a datetime time delta object and return human-readable string"""
    print(timeDiff)

if __name__ == '__main__':
    print(printTable([[1,2,3], [3,2,1]]))
    from collections import OrderedDict
    ods = [OrderedDict(**{'test':1}),
           OrderedDict(**{'test':2, 'hans':3}),
           OrderedDict(**{'test':3, 'mehl': 321412, 'hans':3}),
           ]
    print(printDicts(ods))
    ods.append(OrderedDict(**{'long': ''.join(['{}'.format(i) for i in range(500)])}))
    print(printDicts(ods, width=None))
    print(printDicts(ods, ignoreKeys=["long"]))
    print(printDicts(ods))
    
    import datetime
    days = datetime.timedelta(3)
    print(humanTimeDiff(days))
