#!/usr/bin/env python2.7
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

import re

class ExtractionException(Exception):
    pass

class Sample(object):
    """ class providing a unique interface from sample names """
    def __init__(self, name):
        self.initialExpression = name
        self.datasetType = None
        self._project = None
        self._subProject = None
        self._ID = None
        self._physicsShort = None
        self._streamName = None
        self._prodStep = None
        self._dataType = None
        self._version = None

        self._DID = None
        scopeSplit = name.split(":")
        if len(scopeSplit) == 1:
            self._DID = name
        elif len(scopeSplit) == 2:
            self._DID = scopeSplit[1]
            self._scope = scopeSplit[0]
        else:
            logger.error("DID no assigned")
            raise ExtractionException
        if self._DID.endswith('/'):
            self._DID = self._DID[:-1]
        self._parseSample(self._DID)

    def _parseSample(self, DID):
        """ parsing the sample and identifying the type (https://cds.cern.ch/record/1070318/files/gen-int-2007-001.pdf) """
        if DID[:5] == 'user.':
            self.datasetType = 'user'
            
        elif DID[:6] == 'group.':
            self.datasetType = 'group'
        elif 'PhysCont' in DID:
            self.datasetType = 'container'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._runRange, self._streamName, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
        elif re.search('data\d{2}_calib.', DID) is not None:
            self.datasetType = 'calibration'
        elif re.search('data\d{2}_', DID) is not None:
            self.datasetType = 'realData'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._ID, self._streamName, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
        elif re.search('mc\d{2}_', DID) is not None:
            self.datasetType = 'MC'
            split = DID.split(".")
            assert len(split) == 6
            self._project, self._ID, self._physicsShort, self._prodStep, self._dataType, self._version = split
            self.subProject = self._project.split("_")[1]
        else:
            raise ExtractionException("could not find datasetType for {}".format(DID))
        
    def ID(self):
        return self._ID

    def DID(self):
        """ extract DID from name should not have "scope:*", such that it can be used in rucio and ami """
        return self._DID

    def AOD(self):
        """ extract AOD from name """
        if self._dataType == "AOD":
            return self.DID()
        return DAODtoAOD(self._DID)

    def scope(self):
        """ returns the scope of the sample, e.g. mc15_13TeV or None """
        try:
            return self._scope
        except AttributeError:
            DID = self.DID()
            if DID.startswith('user') or DID.startswith('group'):
                return ".".join(DID.split('.')[0:2])
            else:
                return DID.split('.')[0]

    def __repr__(self):
        lines = []
        lines.append("Sample ({}):".format(self.datasetType))
        lines.append(" - original name: {}".format(self.initialExpression))
        lines.append(" - project: {}".format(self._project))
        lines.append(" - ID: {}".format(self._ID))
        lines.append(" - physics short: {}".format(self._physicsShort))
        lines.append(" - stream: {}".format(self._streamName))
        lines.append(" - production step: {}".format(self._prodStep))
        lines.append(" - data type: {}".format(self._dataType))
        lines.append(" - version: {}".format(self._version))
        return '\n'.join(lines)

# functions that might be interesting standalone
def DAODtoAOD(DID):
    """ get AOD from DAOD """
    outDID, count = re.subn('DAOD_\w{4}\d+', 'AOD', DID)
    if count != 1:
        logger.warning("{} couldn't be replaced to AOD".format(DID))
        raise ExtractionException
    # usually we want to strip one pTag (reprocessed data)
    outDID, pTag = outDID[:-6], outDID[-5:]
    if re.search('p\d{4}', pTag) is None:
        logger.warning("pTag removal failed {}, {}".format(DID, pTag))
        raise ExtractionException
    return outDID

if __name__ == '__main__':
    # some test cases for this class:
    for name in ['mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470', 
                 'mc15_13TeV:mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470', 
                 'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470/', 
                 'mc15_13TeV:mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470/']:
        ttbar = Sample(name)
        print(ttbar)
        assert ttbar.scope() == 'mc15_13TeV', "{}".format(ttbar.scope())
        # assert ttbar._project == 'mc15_13TeV', ttbar._project
        assert ttbar.DID() == 'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_SUSY5.e3698_s2608_s2183_r7267_r6282_p2470'
        assert ttbar.AOD() == 'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7267_r6282'
        assert ttbar.ID() == '410000'

    data15 = Sample("data15_13TeV.00276511.physics_Main.merge.DAOD_SUSY5.r7562_p2521_p2667")
    print(data15)
    assert data15.scope() == 'data15_13TeV'
    assert data15.ID() == '00276511', data15.ID()
    assert data15.DID() == "data15_13TeV.00276511.physics_Main.merge.DAOD_SUSY5.r7562_p2521_p2667"

    for name in ['user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root']:
        cont = Sample(name)
        print(cont)
        assert cont.scope() == 'user.bschacht', "{}".format(ttbar.scope())
        assert cont.DID() == 'user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root'
        try:
            cont.AOD()
            assert False, cont.AOD()
        except ExtractionException:
            pass
        # assert cont.ID() == '00276262', cont.ID()

    print("passed all tests!")
