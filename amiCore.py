from .memeMock import cache

databaseDirectory = "/dev/shm/amiCore"

import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

# Setup the ami clients
import pyAMI.client
import pyAMI.atlas.api as AtlasAPI
client = pyAMI.client.Client('atlas')

AtlasAPI.init()

@cache(cacheDir=databaseDirectory, ttl=36000)
def getDatasetDict(DID, **kwargs):
    """ cached function returning the ami dict for a certain DID or None in case of an exception (contains more information than wildcarded search), example
    OrderedDict([(u'logicalDatasetName', u'mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH.merge.AOD.e4023_s2608_r7725_r7676'), 
    (u'nFiles', u'396'), 
    (u'totalEvents', u'3953000'), 
    (u'totalSize', u'2144367250806'), 
    (u'dataType', u'AOD'), 
    (u'prodsysStatus', u'EVENTS PARTIALLY AVAILABLE'), 
    (u'completion', u'100.00'), 
    (u'ECMEnergy', u'NULL'), 
    (u'physicsComment', u'NULL'), 
    (u'PDF', u'NULL'), 
    (u'version', u'e4023_s2608_r7725_r7676'), 
    (u'AtlasRelease', u'NULL'), 
    (u'crossSection', u'0.69622'), 
    (u'TransformationPackage', u'NULL'), 
    (u'datasetNumber', u'407012'), 
    (u'jobConfig', u'NULL'), 
    (u'principalPhysicsGroup', u'gen-user'), 
    (u'physicsShort', u'PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH'), 
    (u'requestedBy', u'gingrich'), 
    (u'creationComment', u'NULL'), 
    (u'generatorName', u'Powheg+Pythia+EvtGen+Photos+Tauola'), 
    (u'geometryVersion', u'ATLAS-R2-2015-03-01-00'), 
    (u'triggerConfig', u'NULL'), 
    (u'conditionsTag', u'OFLCOND-MC15c-SDR-09'), 
    (u'lastModified', u'2016-06-29 04:10:16'), 
    (u'created', u'2016-03-19 09:00:38'), 
    (u'generatorTune', u'Perugia2012'), 
    (u'amiStatus', u'VALID'), 
    (u'physicsCategory', u'NULL'), 
    (u'physicsProcess', u'NULL'), 
    (u'physicsSubCategory', u'NULL'), 
    (u'trashedBy', u'NULL'), 
    (u'trashDate', u'NULL'), 
    (u'trashAnnotation', u'NULL'), 
    (u'trashTrigger', u'NULL'), 
    (u'createdBy', u'root'), 
    (u'identifier', u'344925'), 
    (u'beamType', u'[collisions]'), 
    (u'modifiedBy', u'amiDataLoad'), 
    (u'productionHistory', u'NULL'), 
    (u'productionStep', u'merge'), 
    (u'projectName', u'mc15_13TeV'), 
    (u'relationalLoaded', u'0'), 
    (u'beam_energy', u'[6500000.0]'), 
    (u'added_comment', u'NULL'), 
    (u'campaign', u'MC15'), 
    (u'subcampaign', u'MC15c'), 
    (u'bunchspacing', u'25ns'), 
    (u'keyword', u'NULL'), 
    (u'prodsysIdentifier_0', u'7965016'), 
    (u'taskStatus_0', u'FINISHED'), 
    (u'TIDState_0', u'added'), 
    (u'task_lastModified_0', u'2016-04-06 11:21:58')])
     """
    try:
        return AtlasAPI.get_dataset_info(client, DID)[0]
    except pyAMI.exception.Error:
        logger.warning("pyAMI error for  {}".format(DID))
        return None

@cache(cacheDir=databaseDirectory, ttl=36000)
def getAmiTag(tag, **kwargs):
    return AtlasAPI.get_ami_tag(client, tag)

if __name__ == "__main__":
    print(getDatasetDict("mc15_13TeV.407012.PowhegPythiaEvtGen_P2012CT10_ttbarMET200_hdamp172p5_nonAH.merge.AOD.e4023_s2608_r7725_r7676"))
