#!/usr/bin/env python
import unittest

from collections import OrderedDict
import commonHelpers.shellUtils as SU
from commonHelpers.html.htmlWriter import HtmlWriter, Tag, Table, Link

class TestHtml(unittest.TestCase):

    def test_Tag(self):
        self.assertEqual(str(Tag('br')), "<br>")
        self.assertEqual(str(Tag('h1', 'text')), "<h1>text</h1>")
        with Tag('p') as p:
            p.add("mehl")
            p("br")
            p.add("lehm")
        self.assertEqual(str(p), "<p>\n mehl\n <br>\n lehm\n</p>")

if __name__ == '__main__':
    unittest.main()
