#!/usr/bin/env python
import unittest
import logging
logger = logging.getLogger(__name__)

from commonHelpers.container import splitBySelectors

class TestContainer(unittest.TestCase):

    def test_splitBySelectors(self):
        # empty container
        self.assertDictEqual(splitBySelectors([], {}), {})
        # basic split
        self.assertDictEqual(splitBySelectors(["mehl"], {"lehm": lambda x: x=='mehl'}), {"lehm": ["mehl"]})
        self.assertDictEqual(splitBySelectors(["mehl", 'helm'], {"lehm": lambda x: x=='mehl'}, addElse=True), {"lehm": ["mehl"], 'else':['helm']})
        with self.assertRaises(IndexError):
            splitBySelectors(["mehl", 'helm'], {"lehm": lambda x: x=='mehl'})
        with self.assertRaises(ValueError):
            splitBySelectors(["mehl", 'helm'], {"lehm": lambda x: True, "mleh": lambda x: True})

if __name__ == '__main__':
    unittest.main()
