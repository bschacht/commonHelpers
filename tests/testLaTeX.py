#!/usr/bin/env python
import unittest
import os
import logging
logger = logging.getLogger(__name__)

from contextlib import contextmanager

import commonHelpers.shellUtils as SU

from commonHelpers.LaTeX import LaTeXWriter, macro, Tabular, Itemize
import commonHelpers.LaTeX as L

compilation = SU.programAvailable('latexmk')
if not compilation:
    logger.warning('no LaTeX compilation, since latexmk is not available')
showPdf = False # turn it on to see the outputs <make configurable>

@contextmanager
def getLW(**packages):
    with SU.tmpDir() as tmpDir:
        with LaTeXWriter(os.path.join(tmpDir, "test"), compilation=compilation, showPdf=showPdf) as lw:
            lw.packages.update(packages)
            with lw() as d:
                yield d


class TestLaTeX(unittest.TestCase):

    def test_LaTeXWriter(self):
        with SU.tmpDir() as tmpDir:
            with LaTeXWriter(os.path.join(tmpDir, "test"), compilation=compilation, showPdf=showPdf) as lw:
            # with LaTeXWriter(os.path.join(tmpDir, "test"), showPdf=True) as lw:
                with lw() as d:
                    d.add(macro("section", "test1"))
                    with d("center") as c:
                        c.add("centered")
            # with open(os.path.join(tmpDir, "test.tex")) as f:
            #     for line in f.readlines():
            #         print(line)

    def test_Tabular(self):
        with SU.tmpDir() as tmpDir:
            with LaTeXWriter(os.path.join(tmpDir, "test"), compilation=compilation, showPdf=showPdf) as lw:
                with lw() as d:
                    with d(Tabular) as t:
                        # logger.info("t {}".format(t))
                        t.addRow("test", "test2")
            # with open(os.path.join(tmpDir, "test.tex")) as f:
            #     for line in f.readlines():
            #         print(line)

    def test_Itemize(self):
        with SU.tmpDir() as tmpDir:
            with LaTeXWriter(os.path.join(tmpDir, "test"), compilation=compilation, showPdf=showPdf) as lw:
                with lw() as d:
                    with d(Itemize) as i:
                        i.addItem("test item")

        with SU.tmpDir() as tmpDir:
            with LaTeXWriter(os.path.join(tmpDir, "test"), compilation=compilation, showPdf=showPdf) as lw:
                with lw() as d:
                    with d(Itemize) as i:
                        i.addItem("test item")
                        i.addItem("another test item")

    def test_others(self):
        """Collecting here environments until they get better test"""
        with getLW(longtable={}) as d:
            with d(L.Longtable) as l:
                l.addRow("hans", "mehl")

        with getLW(adjustbox={}) as d:
            with d(L.Adjustbox) as a:
                a.add("text")

        # with getLW() as d:
        #     with d(L.Frame) as f:
        #         with f(L.Columns) as cs:
        #             with cs(L.Column) as c:
        #                 c.add("text")

    def test_uncertaintiesToRow(self):
        from  commonHelpers.LaTeX.helper import uncertaintiesToRow
        try:
            from uncertainties import ufloat
        except ImportError:
            logger.warning("could not import uncertainties, aborting test!")
            return
        self.assertEqual(uncertaintiesToRow(ufloat(0,0)), ["0", ".", "0", "0", "", ""])
        self.assertEqual(uncertaintiesToRow(ufloat(16,4)), ["16", "", "", "4", "", ""])
        self.assertEqual(uncertaintiesToRow(ufloat(0.1,0.0001)), ["0", ".", "10000", "0", ".", "00010"])
        self.assertEqual(uncertaintiesToRow(ufloat(1000,100)), ["(1", ".", "00", "0", ".", "10) $\\times 10^{3}$"])
        self.assertEqual(uncertaintiesToRow(ufloat(0.000001,0)), ["(1", ".", "0", "0", "", ") $\\times 10^{-6}$"])

        self.assertEqual(uncertaintiesToRow(ufloat(0,0), valueOnly=True), ["0", ".", "0"])
        self.assertEqual(uncertaintiesToRow(ufloat(16,4), valueOnly=True), ["16", "", ""])
        self.assertEqual(uncertaintiesToRow(ufloat(0.1,0.0001), valueOnly=True), ["0", ".", "10000"])
        self.assertEqual(uncertaintiesToRow(ufloat(1000,100), valueOnly=True), ["(1", ".", "00) $\\times 10^{3}$"])
        self.assertEqual(uncertaintiesToRow(ufloat(0.000001,0), valueOnly=True), ["(1", ".", "0) $\\times 10^{-6}$"])

if __name__ == '__main__':
    unittest.main()
