# -*- coding: utf-8 -*-
from toolLogger import mainLogger
logger = mainLogger.getChild(__name__)

from .documentWriter import DocumentWriter
from . import environments as E

class Beamer(DocumentWriter):
    def __init__(self, texPath="/tmp/beamer.tex", **kwargs):
        title = kwargs.pop("title", "")
        author = kwargs.pop("author", None)
        authorShort = kwargs.pop("authorShort", None)
        bottomTitle = kwargs.pop("bottomTitle", "")

        DocumentWriter.__init__(self, texPath, **kwargs)

        self.documentclass = 'beamer'
        self.documentclassOptions= {'10pt':None,
                                    'xcolor':'dvipsnames',
        }
        self.addPreambula("""\mode<presentation>
        {
        \usetheme{Warsaw}
        \setbeamercovered{transparent}
        \setbeamercolor{block title}{fg=structure,bg=white}
        \setbeamertemplate{items}[circle]
        \setbeamertemplate{theorems}[numbered]
        \setbeamertemplate{blocks}[rounded][shadow=false]
        \useoutertheme{infolines}
        \setbeamertemplate{headline}{}
        % \setbeamertemplate{footline}[frame number, infolines]
        \definecolor{links}{HTML}{2A1B81}
        \hypersetup{colorlinks,linkcolor=,urlcolor=links}
        }
        \\beamertemplatenavigationsymbolsempty
        """)
        self.packages["graphicx"] = {}
        self.packages["hyperref"] = {}
        self.packages["soul"] = {}
        self.packages["listings"] = {}
        self.packages["booktabs"] = {}
        self.packages["adjustbox"] = {}

        self.addPreambula(E.macro("title", title, **{bottomTitle: None}))
        if author is not None:
            if authorShort is not None:
                self.addPreambula(E.macro("author", author, **{authorShort:None}))
            else:
                self.addPreambula(E.macro("author", author))
        # self.packages["xcolor"] =["usenames","dvipsnames","svgnames","table"]

    # def addPlots(self, paths, **kwargs):
    #     from helpers import container as C
    #     for i, group in enumerate(C.grouper(4,paths)):
    #         tex = ""
    #         for path1, path2 in C.grouper(2, group):
    #             tex += E.columns(E.inclGraphics(path1, ignoreNone=True), E.inclGraphics(path2, ignoreNone=True))
    #         self.addFrame(tex, kwargs.get("title", "") + " ({})".format(i+1))
