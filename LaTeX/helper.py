# -*- coding: utf-8 -*-
import re

import logging
logger = logging.getLogger(__name__)
import re
import subprocess
from ..memeMock import cache
# get absolute path to database directory
import os, inspect
baseDirectory = os.path.dirname(os.path.abspath(__file__))
databaseDirectory = os.path.join(baseDirectory, '_cache')


class TeXException(Exception):
    pass

def addTimestamp(fn):
    """ Decorator adding timestamp to the first line """
    import datetime
    def inner(*args,**kwargs):
        return "% Created {}\n".format(datetime.date.today()) + fn(*args,**kwargs)
    return inner

def typesetTeX(tex, fileName, directory,  **kwargs):
    import subprocess, os
    import helpers.pathHelpers as PH
    PH.ensurePathExists(directory)
    # write tex file
    texFile = os.path.join(directory, fileName + '.tex')
    with open(texFile, 'w') as f:
        f.write(tex)
    # compile
    if not subprocess.call(['pdflatex', fileName + '.tex'], cwd=directory) == 0:
        logger.error("compilation of {} failed".format(texFile))
        raise TeXException
    else:
        pdfFile = texFile[:-4] + '.pdf'
        logger.info("created: {}".format(pdfFile))
        return pdfFile

def pdflatex(texFile):
    outputDirectory = "/tmp/_autoLaTeX"
    if not os.path.exists(texFile):
        raise IOError("file {} not found".format(texFile))

    # compile
    p = subprocess.Popen(['latexmk', '-interaction=nonstopmode', '-output-directory={}'.format(outputDirectory), '-pdf', os.path.basename(texFile)], cwd=os.path.dirname(texFile) or '.', stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    stdout, stderr = p.communicate()
    retCode = p.returncode
    if not retCode == 0:
        logger.error("compilation of {} failed".format(texFile))
        logger.error("stdout: {}".format(stdout))
        logger.error("stderr: {}".format(stderr))
        raise TeXException
    else:
        pdfFile = texFile[:-4] + '.pdf'
        import shutil
        shutil.move(os.path.join(outputDirectory, os.path.basename(pdfFile)), pdfFile)
        logger.info("created: {}".format(pdfFile))
        return pdfFile

@cache(cacheDir=databaseDirectory)
def testTexString(tex):
    import subprocess
    import helpers.pathHelpers as PH
    testdir = "/tmp/textest/"
    PH.ensurePathExists(testdir)
    testfile = testdir + "texstring.tex"
    with open(testfile, 'w') as f:
        f.write(minimalFile(tex))
    if subprocess.call(['pdflatex', testfile], cwd=testdir)== 0:
        return True
    else:
        logger.error("{} did not compile".format(tex))
        raise TeXException
        return False
    

def transformTLatex(tex, replacements={}):
    tex = "$" + tex + "$"
    for replacement in replacements:
        if re.search(replacement, tex) is not None:
            tex = re.sub(replacement, replacements[replacement], tex)
    if re.search("#", tex) is not None:
        tex = re.sub("#", r'\\', tex)
    if testTexString(tex):
        logger.info("success")
    else:
        logger.info("fail")
    return tex

replacements = {
    'ä':'\\"a',
    'ö':'\\"o',
    'ü':'\\"u',
    'Ä':'\\"A',
    'Ö':'\\"O',
    'Ü':'\\"U',
    '€':'\\euro{}',
    '#':'\\#',
    '_':'\\_',
    '%':'\\%',
    '&':'\\&',
    # '$':'\\$',
    }
def cleanLaTeX(string):
    for repl in replacements:
        string = re.sub(repl, replacements[repl], string)

    return string

def minimalFile(body):
    from .documentWriter import DocumentWriter
    dw = DocumentWriter()
    dw.body = body
    return dw.getTex()

def uncertaintiesToRow(unc, valueOnly=False):
    """takes a ufloat and tranlates it into a list of values to be used in the  "r@{}c@{}l@{\,$\pm$\,}r@{}c@{}l"-scheme """
    row = []
    # four cases to discriminate
    s = "{:.L}".format(unc)
    # print s
    exp = None
    if re.search("times", s) is not None:
        base, exp = s.split("\\times")
        val, err = base.split("\\pm")
        val = val[6:]
        err = err[:-8]
    else:
        val, err = s.split("\\pm")

    # print exp, val, err

    if exp is None:
        row.append(val.split(".")[0].strip())
    else:
        row.append("(" + val.split(".")[0].strip())

    if len(val.split(".")) > 1:
        row.append(".")
        row.append(val.split(".")[1].strip())
    else:
        row += ["", ""]

    if valueOnly:
        if exp is not None:
            row[-1] += ") $\\times{}$".format(exp)
        return row

    if len(err.split(".")) > 1:
        row.append(err.split(".")[0].strip())
        row.append(".")
        row.append(err.split(".")[1].strip())
    else:
        row.append(err.split(".")[0].strip())
        row.append("")
        row.append("")

    if exp is not None:
        row[-1] += ") $\\times{}$".format(exp)
    return row

def wrapTexFile(texfile):
    """setup minimal file to display table in texfile, returns path to pdf"""
    from commonHelpers.LaTeX.LaTeXWriter import LaTeXWriter
    with LaTeXWriter(texfile.replace(".tex", "_wrap.tex")) as lw:
        lw.packages['geometry'] = {'landscape':None}
        lw.packages['booktabs'] = {}
        with lw() as d:
            with open(texfile) as f:
                d.add(f.read())
    return lw.pdfPath
