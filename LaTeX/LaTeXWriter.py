# -*- coding: utf-8 -*-
import logging
logger = logging.getLogger(__name__)

from contextlib import contextmanager

import datetime
import os

from commonHelpers.pathHelpers import ensurePathExists

from .helper import cleanLaTeX, pdflatex
from . import environments as E

def indent(text):
    return " " + "\n ".join(text.split("\n"))

import sys
PY3 = sys.version_info[0] == 3

if PY3:
    string_types = str,
else:
    string_types = basestring,

class LaTeXWriter(object):
    """LaTeXWriter to create and compile latex files with python
    self.packages is a dictionary using the key as package name and the value (a dictionary) as options ([key=value,...])
    """
    def __init__(self, texPath=None, compilation=True, showPdf=False, bodyOnly=False, addTimestamp=True):
        self.documentclass = 'article'
        self.documentclassOptions = {'12pt':None}
        self.packages = {}
        self.preambula = ""

        self.texPath = texPath
        if self.texPath [-4:] != '.tex':
            self.texPath += ".tex"
        self.pdfPath = ''

        self.compilation = compilation
        self.showPdf = showPdf
        self.bodyOnly = bodyOnly
        self.addTimestamp = addTimestamp

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return
        return self.finish()

    def finish(self):
        ensurePathExists(os.path.dirname(self.texPath))
        with open(self.texPath, "w") as f:
            f.write(self.getTex())
        if self.bodyOnly:
            return

        if self.compilation:
            self.pdfPath = self.typeset()

            # show only pdf if it's being compiled
            if self.showPdf:
                import subprocess
                subprocess.call(['evince', self.pdfPath])
        return

    @contextmanager
    def __call__(self, env="document", *args, **kwargs):
        """Call to get the base environment, either supply environment name or supply a custom environment (inherit from Environment)"""
        if isinstance(env, string_types):
            self._body = Environment(env, *args, **kwargs)
        elif issubclass(env, Environment):
            self._body = env(*args, **kwargs)
        else:
            raise TypeError("don't know what to do with {}".format(env))
        yield self._body

    def getTex(self):
        tex = ""
        if self.addTimestamp:
            tex +="% Created {}\n".format(datetime.date.today())

        if self.bodyOnly:
            return tex + str(self._body)
        tex += E.macro("documentclass", self.documentclass, **self.documentclassOptions) + "\n"
        tex += self.preambula
        for package in self.packages:
            tex += E.macro("usepackage", package, **self.packages[package]) + "\n"
        tex += "\n"
        tex += str(self._body)
        return tex

    def typeset(self):
        return pdflatex(self.texPath)

    def addPreambula(self, tex):
        """add latex code to preabula"""
        self.preambula += tex + "\n"

class Environment(object):
    """Environment object translates into LaTeX code"""
    def __init__(self, envName, *args, **kwargs):
        self.envName = envName
        content = kwargs.get("content")
        if content is None:
            self.content = []
        else:
            self.content = [content]
        self.args = list(args)

    def begin(self):
        return "\\begin{{{}}}".format(self.envName) + ''.join(["{{{}}}".format(arg) for arg in self.args])

    def end(self):
        return "\\end{{{}}}".format(self.envName)

    def __str__(self):
        s = self.begin()
        if len(self.content) > 1:
            s += "\n"
            for cont in self.content:
                s += indent(str(cont)) + "\n"
        elif len(self.content) == 1:
            s += str(self.content[0])

        s += self.end()
        return s

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return

    def __call__(self, env=None, *args, **kwargs):
        """Calling an Environment returns a new Environment"""
        if isinstance(env, string_types):
            e = Environment(env, *args, **kwargs)
        elif issubclass(env, Environment):
            e = env(*args, **kwargs)
        else:
            raise TypeError("don't know what to do with {}".format(env))
        self.add(e)
        return e
        
    def add(self, cont):
        """Add content here, for creation of tex str(cont) will be called"""
        self.content.append(cont)

def macro(macroName, *args, **options):
    """Return string of a LaTeX macro

    \macroName[opt1,opt2=val2,...]{arg1}{arg2}...
    args: will be used as content
    options with None as value will be used as opt1
    options with value will be used as opt2"""

    tex = '\\{}'.format(macroName)

    opts = []
    for option, val in options.items():
        if val is not None:
            opts.append("{}={}".format(option,val))
        else:
            opts.append("{}".format(option))

    if opts:
        tex += "[" + ", ".join(opts) + "]"

    for arg in args:
        tex += '{{{}}}'.format(arg)
    return tex

class Beamer(LaTeXWriter):
    def __init__(self, title="", bottomTitle="", author=None, authorShort=None, **kwargs):
        """Beamer class inherits from LaTeXWriter
        """
        LaTeXWriter.__init__(self, **kwargs)

        self.documentclass = 'beamer'
        self.documentclassOptions= {'10pt':None,
                                    'xcolor':'dvipsnames',
        }
        self.addPreambula("""\mode<presentation>
        {
        \\usetheme{Warsaw}
        \setbeamercovered{transparent}
        \setbeamercolor{block title}{fg=structure,bg=white}
        \setbeamertemplate{items}[circle]
        \setbeamertemplate{theorems}[numbered]
        \setbeamertemplate{blocks}[rounded][shadow=false]
        \\useoutertheme{infolines}
        \setbeamertemplate{headline}{}
        % \setbeamertemplate{footline}[frame number, infolines]
        \definecolor{links}{HTML}{2A1B81}
        \hypersetup{colorlinks,linkcolor=,urlcolor=links}
        }
        \\beamertemplatenavigationsymbolsempty
        """)
        self.packages["graphicx"] = {}
        self.packages["hyperref"] = {}
        self.packages["soul"] = {}
        self.packages["listings"] = {}
        self.packages["booktabs"] = {}
        self.packages["adjustbox"] = {}

        self.addPreambula(E.macro("title", title, **{bottomTitle: None}))
        if author is not None:
            if authorShort is not None:
                self.addPreambula(E.macro("author", author, **{authorShort:None}))
            else:
                self.addPreambula(E.macro("author", author))
    # def addPlots(self, paths, **kwargs):

    #     from helpers import container as C
    #     for i, group in enumerate(C.grouper(4,paths)):
    #         tex = ""
    #         for path1, path2 in C.grouper(2, group):
    #             tex += E.columns(E.inclGraphics(path1, ignoreNone=True), E.inclGraphics(path2, ignoreNone=True))
    #         self.addFrame(tex, kwargs.get("title", "") + " ({})".format(i+1))
