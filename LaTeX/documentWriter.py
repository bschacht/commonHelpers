# -*- coding: utf-8 -*-
from toolLogger import mainLogger
logger = mainLogger.getChild(__name__)

from contextlib import contextmanager

import datetime
import os

from .helper import cleanLaTeX, pdflatex
from . import environments as E

from commonHelpers.pathHelpers import ensurePathExists
logger.warning("this tool should be deprecated!")

class DocumentWriter(object):
    def __init__(self, texPath="/tmp/tmp.tex", cleanup=True, compilation=True, showPdf=False, bodyOnly=False, addTimestamp=True):
        self.submodules = []
        self.additionalHeader = ''
        self.preambula = ''
        self.body = ''
        self.documentclass = 'article'
        self.documentclassOptions = {'12pt':None}
        self.packages = {}

        self.texPath = texPath
        if self.texPath [-4:] != '.tex':
            self.texPath += ".tex"
        self.pdfPath = ''

        self.compilation = compilation
        self.showPdf = showPdf
        self.bodyOnly = bodyOnly
        if addTimestamp:
            self.add("% Created {}\n".format(datetime.date.today()))

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return
        
        ensurePathExists(os.path.dirname(self.texPath))
        with open(self.texPath, "w") as f:
            f.write(self.getTex())
        if self.bodyOnly:
            return

        if self.compilation:
            self.pdfPath = self.typeset()
        if self.showPdf:
            import subprocess
            subprocess.call(['evince', self.pdfPath])
        return self.pdfPath

    def add(self, tex):
        self.body += tex + '\n'

    def addPreambula(self, tex):
        self.preambula += tex + '\n'

    @contextmanager
    def addEnv(self, Env, *args, **kwargs):
        e = Env(dw=self, *args, **kwargs)
        yield e
        self.add(str(e))

    def getTex(self):
        if self.bodyOnly:
            return self.body
        tex = E.macro("documentclass", self.documentclass, **self.documentclassOptions) + "\n"
        for package in self.packages:
            tex += E.macro("usepackage", package, **self.packages[package]) + "\n"
        tex += self.preambula
        tex += "\n"
        tex += "\\begin{document}\n"
        tex += self.body
        tex += "\\end{document}\n"
        return tex

    def typeset(self):
        return pdflatex(self.texPath)
