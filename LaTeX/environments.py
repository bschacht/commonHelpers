# -*- coding: utf-8 -*-
from __future__ import print_function
import logging
logger = logging.getLogger(__name__)
from contextlib import contextmanager
from commonHelpers.interaction import tableFromDicts

logger.warning("this tool should be deprecated!")
class Environment(object):
    def __init__(self, envName, *args, **kwargs):
        dw = kwargs.pop("dw", None)
        body = kwargs.pop("body", None)
        self.envName = envName
        self.dw = dw
        self.args = list(args)
        self._body = ''
        if body is not None:
            self._body += body

    def begin(self):
        return "\\begin{{{}}}".format(self.envName) + ''.join(["{{{}}}".format(arg) for arg in self.args])

    def end(self):
        return "\\end{{{}}}".format(self.envName)

    def add(self, tex):
        self._body += tex + "\n"

    def body(self):
        """ Can be overwritten by derived classes """
        return self._body

    def __repr__(self):
        """ return the string in case we are giving the class to format """
        return self.begin() + '\n' + self.body() + '\n' + self.end()

    def __enter__(self):
        if self.dw is None:
            raise TypeError("context manager of environment only makes sense with DocumentWriter attached")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.dw.add("{}".format(self))

    @contextmanager
    def addEnv(self, Env, *args, **kwargs):
        e = Env(dw=self.dw, *args, **kwargs)
        yield e
        self.add(str(e))

def macro(macroName, *args, **options):
    tex = '\\{}'.format(macroName)

    opts = []
    for option, val in options.items():
        if val is not None:
            opts.append("{}={}".format(option,val))
        else:
            opts.append("{}".format(option))

    if opts:
        tex += "[" + ", ".join(opts) + "]"

    for arg in args:
        tex += '{{{}}}'.format(arg)
    return tex

# custom environments
class Letter(Environment):
    def __init__(self, adress, dw=None, subject='', opening='', closing='Mit freundlichen Grüßen,'):
        super(Letter, self).__init__("letter", adress, dw=dw)

        self.add(macro("subject", subject))
        self.add(macro("opening", opening))

        self.closing = closing

    def __exit__(self, *args):
        self.add(macro("closing", self.closing))
        return super(Letter, self).__exit__(*args)

class Tabular(Environment):
    def __init__(self, rows=[], dw=None):
        super(Tabular, self).__init__("tabular", dw=dw)

        self.rowLength = None
        for row in rows:
            self.addRow(row)

    def begin(self):
        self.args.append("l"*self.rowLength)
        return super(Tabular, self).begin()

    def add(self, tex):
        raise NotImplementedError("tabular should not be used with add")

    def addRow(self, row):
        if self.rowLength is None:
            self.rowLength = len(row)

        if len(row) != self.rowLength:
            raise IndexError('Only equal length rows are allowed')

        self._body += "&".join(row) + '\\\\\n'

    def addMultiColumn(self, row):
        """ Add a row of style: [(nCells, content), ...] """
        if self.rowLength is None:
            print("set row length")
            self.rowLength = sum([i for i, _ in row])

        print( row, sum([i for i, _ in row]), self.rowLength)
        if sum([i for i, _ in row]) != self.rowLength:
            raise IndexError('Only equal length rows are allowed')
        cells = []
        for nCells, content in row:
            cells.append(macro("multicolumn", nCells, "c", content))
        self._body += "&".join(cells) + '\\\\\n'

    def addDictionaries(self, *ds):
        self.addTopRule()
        firstLine = True
        for row in tableFromDicts(ds):
            self.addRow(row)
            if firstLine:
                self.addMidRule()
                firstLine = False
        self.addBottomRule()

    def addTopRule(self):
        self._body += macro("toprule") + '\n'

    def addMidRule(self):
        self._body += macro("midrule") + '\n'

    def addBottomRule(self):
        self._body += macro("bottomrule") + '\n'

class Itemize(Environment):
    def __init__(self, dw=None):
        super(Itemize, self).__init__("itemize", dw=dw)

    def addItem(self, text):
        self._body += "\\item {}\\\\\n".format(text)

class Longtable(Tabular):
    def __init__(self, *args, **kwargs):
        super(Longtable, self).__init__(*args, **kwargs)
        self.envName = "longtable"

class Adjustbox(Environment):
    def __init__(self, maxWidth="0.99\\textwidth", dw=None):
        super(Adjustbox, self).__init__("adjustbox", "max width={}".format(maxWidth), dw=dw)

    def addBottomRule(self):
        self._body += macro("bottomrule") + '\n'

class Adjustbox(Environment):
    def __init__(self, maxWidth="0.99\\textwidth", maxHeight="0.8\\textheight", dw=None):
        if dw is not None:
            dw.packages['adjustbox'] = {}
        super(Adjustbox, self).__init__("adjustbox", "max width={}, max totalheight={}".format(maxWidth, maxHeight), dw=dw)

class Frame(Environment):
    def __init__(self, dw=None, title=None):
        super(Frame, self).__init__("frame", dw=dw)
        if title is not None:
            self.add(macro("frametitle", title))

class Columns(Environment):
    def __init__(self, dw=None):
        super(Columns, self).__init__("columns", dw=dw)

class Column(Environment):
    def __init__(self, dw=None, width="0.48\\textwidth"):
        super(Column, self).__init__("column", width, dw=dw)
    
        
# custom macros
def includeGraphics(path, ignoreNone=False, width="\\textwidth", height="\\textheight", keepaspectratio=True):
    import os.path
    if path is None and ignoreNone:
        return " "
    if not os.path.exists(path):
        raise IOError
    return macro("includegraphics", path, width=width, height=height, keepaspectratio=keepaspectratio)

def vspace(i, unit="em"):
    return macro("vspace", "{}{}".format(i, unit))

