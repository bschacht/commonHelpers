from .documentWriter import DocumentWriter

class LetterWriter(DocumentWriter):
    def __init__(self, *args, **kwargs):
        """ Kwargs on top of documentWriter:
        address
        place
        signature
        """
        address = kwargs.pop("address")
        place = kwargs.pop("place")
        signature = kwargs.pop("signature")

        super(LetterWriter, self).__init__(*args, **kwargs)
        d.documentclass = 'dinbrief'
        d.packages = {
            "inputenc": {"utf8":None},
            "fontenc": {"T1":None},
            "graphicx": {},
            "xcolor": {},
            "ngerman": {},
            "eurosym": {"official":None},
        }
        d.addPreambula(macro('address', address))
        d.addPreambula(macro('place', place))
        d.addPreambula(macro('signature', signature))
